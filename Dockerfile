#FROM ubuntu:16.04
#FROM python:3.7-slim-buster

FROM  python:3.7-slim-stretch
USER root
RUN mkdir -p /usr/share/man/man1 
RUN apt update && apt install -y --no-install-recommends openjdk-8-jdk
RUN apt-get update && \
  apt-get upgrade -y && \
  apt-get install -q -y supervisor wget tar bash iputils-ping telnet

#RUN wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add -
#RUN add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/
#RUN apt-get install adoptopenjdk-8-hotspot








#RUN pip3 install --upgrade pip jupyter
#RUN pip3 install --upgrade pip pyspark
#RUN pip3 install --upgrade pip py4j

RUN wget -c https://mirrors.estointernet.in/apache/spark/spark-2.4.5/spark-2.4.5-bin-hadoop2.7.tgz


RUN tar -xvf spark-2.4.5-bin-hadoop2.7.tgz && mv spark-2.4.5-bin-hadoop2.7 /spark && rm spark-2.4.5-bin-hadoop2.7.tgz
COPY start-master.sh /start-master.sh
COPY start-worker.sh /start-worker.sh

ENV SPARK_HOME='/spark'
ENV PATH=$SPARK_HOME:$PATH
ENV PYTHONPATH=$SPARK_HOME/python:$PYTHONPATH

#RUN /bin/bash -c "source ~/.bashrc"
#RUN mkdir /jupyter && \
#    mkdir /jupyter/notebooks
#WORKDIR /jupyter/

RUN apt-get clean

#CMD ["sh","-c","jupyter notebook --ip 0.0.0.0 --allow-root --no-browser"]

