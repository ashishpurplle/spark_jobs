
# coding: utf-8

# In[ ]:

import libs.mysql.connector
# import psycopg2
from libs.mysql.connector import Error
import os
import re
# from sqlalchemy import create_engine
import pandas as pd
conn=False
# from google.cloud import bigquery
# from google.oauth2 import service_account

from pyspark.sql import SQLContext

import resource
resource.getrusage(resource.RUSAGE_SELF).ru_maxrss

class getConnection(object):
    def __init__(self,sc):
        print("in getConnection constru")
        self.sqlContext = SQLContext(sc)

    def get_mysql_instance (self,database,engine="") :
        conn=False
        try:
            if engine == 'spark':
                if database == "warehouse" :
                    conn = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(os.environ['WH_SERVER_ADDRESS'],3306, os.environ['WH_DB_NAME'],os.environ['WH_USER_NAME'],os.environ['WH_PASSWORD'])
                elif database == "slave" :
                    conn = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(os.environ['SERVER_ADDRESS'],3306, os.environ['DB_NAME'],os.environ['USER_NAME'],os.environ['PASSWORD'])
                elif database == "delayed_slave" :
                    conn = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(os.environ['DELAYED_SLAVE_ADDRESS'],3306, os.environ['DELAYED_SLAVE_DB_NAME'],os.environ['DELAYED_SLAVE_USER_NAME'],os.environ['DELAYED_SLAVE_PASSWORD'])
                elif database == 'sandbox' :
                    conn = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(os.environ['SB_SERVER_ADDRESS'],3306, os.environ['SB_DB_NAME'],os.environ['SB_USER_NAME'],os.environ['SB_PASSWORD'])
                else :
                    conn=False
            else:
                if database == "warehouse" :
                    conn = mysql.connector.connect(host=os.environ['WH_SERVER_ADDRESS'],database=os.environ['WH_DB_NAME'],user=os.environ['WH_USER_NAME'],password=os.environ['WH_PASSWORD'],ssl_disabled='True')
                elif database == "slave" :
                    conn = mysql.connector.connect(host=os.environ['SERVER_ADDRESS'],database=os.environ['DB_NAME'],user=os.environ['USER_NAME'],password=os.environ['PASSWORD'],ssl_disabled='True')
                elif database == "delayed_slave" :
                    conn = mysql.connector.connect(host=os.environ['DELAYED_SLAVE_ADDRESS'],database=os.environ['DELAYED_SLAVE_DB_NAME'],user=os.environ['DELAYED_SLAVE_USER_NAME'],password=os.environ['DELAYED_SLAVE_PASSWORD'],ssl_disabled='True')
                elif database == "redshift" :
                    try:
                        conn = psycopg2.connect(host=os.environ['PGHOST'],database=os.environ['PGDATABASE'],user=os.environ['PGUSER'],password=os.environ['PGPASSWORD'],port=os.environ['PGPORT'])
                    except:
                        print("I am unable to connect to the database")
                elif database == 'sandbox' :
                    conn = mysql.connector.connect(host=os.environ['SB_SERVER_ADDRESS'],database=os.environ['SB_DB_NAME'],user=os.environ['SB_USER_NAME'],password=os.environ['SB_PASSWORD'],ssl_disabled='True')

                else :
                    conn=False
            return conn
        except Error as e:
            print(e)
            return False


    def get_db_instance (database) :
        try:
            if database == "warehouse" :
                conn = create_engine('mysql+mysqlconnector://%s:%s@%s/%s' % (os.environ['WH_USER_NAME'],os.environ['WH_PASSWORD'],os.environ['WH_SERVER_ADDRESS'],os.environ['WH_DB_NAME']), echo=False)
            elif database == "slave" :
                conn = create_engine('mysql+mysqlconnector://%s:%s@%s/%s' % (os.environ['USER_NAME'],os.environ['PASSWORD'],os.environ['SERVER_ADDRESS'],os.environ['DB_NAME']), echo=False)
            elif database == "delayed_slave" :
                conn = create_engine('mysql+mysqlconnector://%s:%s@%s/%s' % (os.environ['DELAYED_SLAVE_USER_NAME'],os.environ['DELAYED_SLAVE_PASSWORD'],os.environ['DELAYED_SLAVE_ADDRESS'],os.environ['DELAYED_SLAVE_DB_NAME']), echo=False)
            elif database == "redshift" :
                try:
                    conn = psycopg2.connect(host=os.environ['PGHOST'],database=os.environ['PGDATABASE'],user=os.environ['PGUSER'],password=os.environ['PGPASSWORD'],port=os.environ['PGPORT'])
                except:
                    print("I am unable to connect to the database")
            elif database == "sandbox" :
                conn = create_engine('mysql+mysqlconnector://%s:%s@%s/%s' % (os.environ['SB_USER_NAME'],os.environ['SB_PASSWORD'],os.environ['SB_SERVER_ADDRESS'],os.environ['SB_DB_NAME']), echo=False)
            else:
                conn=False
            return conn
        except Error as e:
            print(e)
            return False


    def set_limits (sql,db):
            try:
                    conn=get_mysql_instance(db)
                    cursor = conn.cursor()
                    cursor.execute(sql)
                    return True
            except Error as e:
                    print(e)
                    return False

    def select_data(sql,data,db) :
        try:
            conn=get_mysql_instance(db)
            cursor = conn.cursor()
            cursor.execute("SET SESSION group_concat_max_len = 10000000;")
            cursor.execute(sql,data)
            rows = cursor.fetchall()
            return rows
        except Error as e:
            print(e)
            return []

    def fetch_data(sql,db) :
            try:
                if db == "bigquery" :
                    credentials = service_account.Credentials.from_service_account_file('/home/purplle/recoEnginePyhton/helper/mysql/DataPipelineProduction-8521e6e33aff.json')
                    client = bigquery.Client(credentials= credentials,project='datapipelineproduction')
                    dir(client)
                    #dataset_ref = client.dataset('event', project='datapipelineproduction')
                    sql = sql.replace("event.", os.environ['dataset']+".")
                    df = client.query(sql).to_dataframe()
                    return df
                else :
                        conn=get_mysql_instance(db)
                        cursor = conn.cursor()
                        cursor.execute(sql)
                        rows = cursor.fetchall()
                        return rows
            except Error as e:
                    print(e)
                    return []




    def fetch_data_with_schema(self,sql,db) :
            try:
                if db == "bigquery" :
                    credentials = service_account.Credentials.from_service_account_file('/home/purplle/recoEnginePyhton/helper/mysql/DataPipelineProduction-8521e6e33aff.json')
                    client = bigquery.Client(credentials= credentials,project='datapipelineproduction')
                    dir(client)
                    #dataset_ref = client.dataset('event', project='datapipelineproduction')
                    sql = sql.replace("event.", os.environ['dataset']+".")
                    df = client.query(sql).to_dataframe()
                    return df
                else :
                        print("in fetch_data_with_schema DB->>>>",db)
                        conn=self.get_mysql_instance(db,"spark")
                        # conn=get_mysql_instance(db)
                        query = "("+sql+") t1_alias"
                        # cursor = conn.cursor()
                        # cursor.execute(sql)
                        # rows = cursor.fetchall()
                        # df=pd.DataFrame(rows,columns=cursor.column_names)
                        df = self.sqlContext.read.format('jdbc').options(driver = 'com.mysql.jdbc.Driver',url=conn, dbtable=query ).load()
                        print(df)
                        # df = df.toPandas() 
                        print(df)
                        return df
            except Error as e:
                    print(e)
                    return []



    def fetch_dataframe(sql,db):
            try:
                    con=get_mysql_instance(db)
                    data=pd.read_sql(sql,con=con)
                    return data
            except Error as e:
                    print(e)
                    return []


    def insert_dataframe_to_db(dataframe,db,tablename,if_exists='append'):
        try:
            con=get_db_instance(db)
            dataframe.to_sql(name=tablename, con=con, if_exists=if_exists, index=False)
            return
        except Error as e:
            print(e)
            return

    def update_data(sql,data,db) :
        try :
            conn=get_mysql_instance(db)
            cursor=conn.cursor()
            cursor.execute(sql,data)
            conn.commit()
            return cursor.rowcount
        except Error as e:
            print(e)
            return False    

    def insert_data(sql,db):
        try :
            conn=get_mysql_instance(db)
            cursor=conn.cursor()
            cursor.execute(sql)
            conn.commit()
            return cursor.rowcount
        except Error as e:
            print(e)
            return False

    def truncate_table(table_name,db):
        try :
            conn=get_mysql_instance(db)
            cursor=conn.cursor()
            cursor.execute("truncate table "+table_name)
            conn.commit()
            return True
        except Error as e:
            print(e)
            return False

    def get_content_from_database(source_info) :
        doc_text=""
        success_flag=False
        conn=get_mysql_instance("slave")
        cursor = conn.cursor()
        sql = "select "+source_info[3]+" from  product_product WHERE id="+str(source_info[6])
        cursor.execute(sql)
        row = cursor.fetchone() 
        if len(row) > 0 :
            if row[0] == None :
                doc_text=""
            else :
                doc_text=row[0]
                success_flag=True
        doc_text=re.sub('<[A-Za-z\/][^>]*>', '', doc_text)

        if source_info[3] == "features" : 
            doc_text=doc_text.replace('\n', '.').replace('\r', '').replace('\t','').strip()
        doc_text=doc_text.replace('\n', '').replace('\r', '').replace('\t','').strip()
        target=open(os.environ['KNOWLEDGE_ENGINE']+'data/raw','w+')
        target.write(doc_text)
        target.close
        return success_flag

if __name__ == '__main__':       # when run as a script
    print(get_mysql_instance("sandbox"))
    #print(fetch_data("select count(*) from `event.buy`", "bigquery"))
# get_connection = getConnection()



