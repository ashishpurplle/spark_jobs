import os
DEBUG = os.environ.get('DEBUG', True)
SECRET_KEY = os.environ.get('FLASK_SECRET', os.environ['FLASK_SECRET'])
API_TOKEN = os.environ.get('API_TOKEN', os.environ['API_TOKEN'])
APP_HOST = os.environ.get('APP_HOST', os.environ['APP_HOST'])
APP_PORT = os.environ.get('APP_PORT', 8014)
REDIS_HOST = os.environ.get('REDIS_HOST', os.environ['REDIS_HOST'])
REDIS_PORT = os.environ.get('REDIS_PORT', os.environ['REDIS_PORT'])
REDIS_DB = os.environ.get('REDIS_DB', 2)
