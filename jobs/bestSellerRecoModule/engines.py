
# coding: utf-8

# In[ ]:

import time
import sys,os
sys.path.append(os.environ['ENGINE_HOME'])
import src.main.dataSource as DS
app.config.from_object('settings')


class ContentEngine(object):

    """
    SIMKEY => p:recoid_model:%s
    TSSIMKEY => tk:p:id_model:%s
    bs|p:bs:%s|tk:p:bs:%s|m1|basic_algo
    """
    
    def __init__(self):
        df_config_param = pd.read_csv('config_param',sep='|')
        setattr(self,'number_of_models',len(df_config_param))
        for  id , row in df_config_param.iterrows():
            setattr(self,'model_info_for_'+row['model_id'],row)        
        self._r = redis.StrictRedis(host=current_app.config['REDIS_HOST'], port=current_app.config['REDIS_PORT'], db=current_app.config['REDIS_DB'])
    def train(self,input_data):
        DS.train(self,input_data)


# In[ ]:

content_engine = ContentEngine()


# In[ ]:

engine= ContentEngine()



def basic_algo(model_info) :
        print("calling basic algo")

def eval_algo(model_info) :
        print("calling eval algo")
        print(model_info)


# In[ ]:

options = {
    'basic_algo' : basic_algo,
    'eval_algo' : eval_algo


}

