
# coding: utf-8

# In[ ]:
import pandas as pd
import time
import sys,os
# sys.path.append(os.environ['ENGINE_HOME'])
# print(sys.path)
from src.main import dataSource as DS
from pyspark.sql import SQLContext

# app.config.from_object('settings')


class bestSellerRecoModule(object):
    def __init__(self,sc):
        self.sc = sc
        df_config_param = pd.read_csv('/home/ashish/spark_jobs/jobs/bestSellerRecoModule/config_param',sep='|')
        setattr(self,'number_of_models',len(df_config_param))
        for  id , row in df_config_param.iterrows():
            setattr(self,'model_info_for_'+row['model_id'],row)        
    
    def train(self,input_data):
        DS.train(self,self.sc,input_data)


