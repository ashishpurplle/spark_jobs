from flask_api import FlaskAPI
from flask import request, current_app, abort
from functools import wraps

import sys,os
sys.path.append(os.environ['ENGINE_HOME'])
import helper.mysql.get_connection  as SQL
import helper.mail.send_mail as EA

app = FlaskAPI(__name__)
app.config.from_object('settings')



def token_auth(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if request.headers.get('X-API-TOKEN', None) != current_app.config['API_TOKEN']:
            abort(403)
        return f(*args, **kwargs)
    return decorated_function


@app.route('/predict', methods=['POST'])
@token_auth
def predict():
	try :
		from src.main.serving import serve_engine
		item = request.data.get('catid')
		num_predictions = 16
		if not item:
			item="home"
		return serve_engine.predict(str(item), num_predictions)
	except :
		EA.send_mail(sys.exc_info(),"serving","bs",os.environ['RECO_ENV'])
		return {"status":"0","message":"serving failed"}
@app.route('/train')
@token_auth
def train():
	try :
		from engines import content_engine
#    data_url = request.data.get('data-url', None)
		data_url= request.args.to_dict()
		print(data_url)
		content_engine.train(data_url)
		return {"message": "Success!", "success": 1}
	except :
		EA.send_mail(sys.exc_info(),"train","bestSellerRecoModule",os.environ['RECO_ENV'])

if __name__ == '__main__':
    app.debug = True
    app.run(host= app.config['APP_HOST'], port= app.config['APP_PORT'])
