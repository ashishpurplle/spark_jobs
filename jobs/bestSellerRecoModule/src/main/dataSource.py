
# coding: utf-8

# In[ ]:

import time
import sys, os
# sys.path.append(os.environ['ENGINE_HOME'])
# import shared.get_connection as SQL
import algorithm as AL
import ast
import numpy as np
from pyspark.sql import SQLContext
from pyspark.sql.functions import coalesce,col,lit,round
from pyspark.sql.types import *

def info(msg):
    print msg


def get_ipo_drr_score(row):
    ip_drr_score = 0
    if row['drr'] > 0:
        ip_drr_score = row['ipo'] / row['drr']
    return float(ip_drr_score)


options = {'basic_algo': AL.basic_algo,
 'eval_algo': AL.eval_algo}

def train(self,sc, input_data):
    from shared.get_connection import getConnection
    SQL = getConnection(sc)
    start = time.time()
    widget = input_data['widget']
    model = input_data['model']
    df_datasource_config = SQL.fetch_data_with_schema("select * from ml_datasource_config where widget='" + widget + "' and model='" + model + "'", 'warehouse')
    df_algorithm_config = SQL.fetch_data_with_schema("select * from ml_algorithm_config where widget='" + widget + "' and model='" + model + "'", 'warehouse')
    df_datasource = {}
    model_root_element_key = df_algorithm_config['model_root_element_key'][0]
    home_element_key_id = df_algorithm_config['home_element_key_id'][0]
    for row in df_datasource_config.rdd.collect():
        print 'fetching dataframe %s ...' % row['data_id']
        df_datasource[row['data_id']] = SQL.fetch_data_with_schema(row['data_query'], row['query_database'])
        print 'count for dataframe %s ' % df_datasource[row['data_id']].count()
        if row['data_id'] != 'product_pool':
            df_datasource[row['data_id']] = df_datasource[row['data_id']].join(df_datasource['product_pool'], on=['product_id'],how="left")
            # df_datasource[row['data_id']] = pd.merge(df_datasource[row['data_id']], df_datasource['product_pool'][['product_id', model_root_element_key]], on=['product_id'], how='left')
            # df_datasource[row['data_id']][[model_root_element_key]] = df_datasource[row['data_id']][[model_root_element_key]].fillna(-1).astype(int)
            df_datasource[row['data_id']] = df_datasource[row['data_id']].fillna(-1)
    print(df_datasource['ctb_data'].show())
    if 'ctb_data' in df_datasource:
        # print(df_datasource['ctb_data']["ctr"])
        df_datasource['ctb_data'] = df_datasource['ctb_data'].withColumn("ctr",round(df_datasource['ctb_data']["ctr"], 0)) 
        df_datasource['ctb_data'] = df_datasource['ctb_data'].withColumn("ctb_percentage",round(df_datasource['ctb_data']["ctb_percentage"], 0)) 
        # df_datasource['ctb_data'].ctr = df_datasource['ctb_data'].ctr.round()
        # df_datasource['ctb_data'].ctb_percentage = df_datasource['ctb_data'].ctb_percentage.round()
        # df_datasource['ctb_data'] = df_datasource['ctb_data'].withColumn("ctb_percentage",coalesce(df_datasource['ctb_data'].ctb_percentage,df_datasource['ctb_data'].ctb_percentage.round())) 
        print(df_datasource['ctb_data'].show())
        for model_id in range(1, self.number_of_models + 1):
            model_info = getattr(self, 'model_info_for_m' + str(model_id))
            options[model_info['algo_method']](self, model_info, df_datasource, df_datasource_config, df_algorithm_config, model, widget)

    info('Engine trained in %s seconds.' % (time.time() - start))


