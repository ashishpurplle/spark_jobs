
# coding: utf-8

# In[1]:

import time
import sys, os
# sys.path.append(os.environ['ENGINE_HOME'])
from shared import get_connection as SQL
# import helper.lib.Evaluation as EV
#import helper.mysql.get_connection as SQL
# import helper.google_cloud.bigTable_connector as BT
import ast
import numpy as np
# import helper.lib.utilities as UT 
from pyspark.sql import SQLContext
from pyspark.sql.functions import coalesce,col,lit,udf,monotonically_increasing_id,row_number,pandas_udf,PandasUDFType,struct
from pyspark.sql.types import *
from functools import reduce
from pyspark.sql import Window 



def info(msg):
    print(msg)
    # current_app.logger.info(msg)

def calculate_freq(toal,sequence_number):
    return float(sequence_number) / float(total) * 100

# @pandas_udf("category_id int, product_id bigint, gmv", PandasUDFType.GROUPED_MAP)
# def frequencies(ds):
   
#     # ds = ds.withColumn('sequence_number', row_number().over(Window.partitionBy('category_id').orderBy(by_key)))
#     # # print(ds.show())
#     # total = float(ds.count())
#     # # print("total------------>",total)
#     # # print(ds.dtypes)
#     # df_with_cat = ds.withColumn(to_key,((ds.sequence_number) / (total) * 100))
    
#     # df_with_cat = df_with_cat.drop('sequence_number')
#     # df_with_cat = df_with_cat.drop(by_key)
#     # print(df_with_cat.show())
#     return ds
def frequencies(ds,by_key,to_key):
   
    ds = ds.withColumn('sequence_number', row_number().over(Window.partitionBy('category_id').orderBy(by_key)))
    # print(ds.show())
    total = float(ds.count())
    # print("total------------>",total)
    # print(ds.dtypes)
    df_with_cat = ds.withColumn(to_key,((ds.sequence_number) / (total) * 100))
    
    df_with_cat = df_with_cat.drop('sequence_number')
    df_with_cat = df_with_cat.drop(by_key)
    # print(df_with_cat.show())
    return df_with_cat


mapping_dic={"category_id":"brand_id","brand_id":"category_id"}

def getRelativeFrequencyFor(ds, for_key, on_key, value_key):
    start = time.time()

    root_element_id_with_products = {}
    # print(ds.rdd.map(lambda row:(row[for_key],row[on_key],row[value_key])).take(10))
    # print(ds.rdd.map(lambda row:(row[for_key],row[on_key],row[value_key])).take(10).map { case (row[for_key], row[on_key], row[value_key]) => ((row[for_key], row[on_key]), row[value_key]) }.reduceByKey(_ + _).map { case ((row[for_key], row[on_key]), row[value_key]) => (row[for_key], (row[on_key], row[value_key])) }.groupByKey())
    # print(ds.show())
    # udfValueToCategory = udf(frequencies)
    # df_with_cat = ds.withColumn("freq", udfValueToCategory(value_key))
    # # ds= ds.rdd.withColumn("freq", frequencies(col(value_key))) 
    # print(df_with_cat.show())
    # exit()
    for row in ds.rdd.collect():
        if row[for_key] not in root_element_id_with_products:
            root_element_id_with_products[row[for_key]] = []
        root_element_id_with_products[row[for_key]].append([row[on_key], row[value_key]])
    # print(root_element_id_with_products)
    
    # start = time.time()
    # list_persons = map(lambda row: row.asDict(), ds.collect())

    # dict_persons = {person[for_key]: list([person[on_key],person[value_key]]) for person in list_persons}
    # print(dict_persons)
    # end = time.time()
    # print "\nExecution of job took %s seconds" % (end-start)
    # exit()

    for cat_key, cat_products in root_element_id_with_products.items():
        rank_score = []
        print(cat_key)
        # print(cat_products)
        for product_details in cat_products:
            print(product_details)
            rank_score.append(product_details[1])
        print("rank_score------------------------------------------",rank_score)

        rank_score_frequencies = frequencies(rank_score)
        product_details_with_frequency_score = []
        for product_details in cat_products:
            product_details.append(rank_score_frequencies[product_details[1]])
            product_details_with_frequency_score.append(product_details)

        root_element_id_with_products[cat_key] = product_details_with_frequency_score
        print(root_element_id_with_products)
        exit()
    end = time.time()

    print "\nExecution of job %s took %s seconds" % (value_key,end-start)
    return root_element_id_with_products


def get_reco_score(row):
    if row is not None and row.gmv_current_score is not None and row.gmv_lifetime_score is not None and row.buy_quantity_current_score is not None and row.buy_quantity_lifetime_score is not None:
        return float((row['gmv_current_score'] * 0.25 + row['gmv_lifetime_score'] * 0.25 + row['buy_quantity_current_score'] * 0.25 + row['buy_quantity_lifetime_score'] * 0.25) / 100)
    else:
        return None
     


def get_perfomence_score(row):
    if row is not None and row.gmv_current_score is not None and row.gmv_lifetime_score is not None and row.buy_quantity_current_score is not None and row.buy_quantity_lifetime_score is not None:
        return float((row['ctb_lifetime_score'] * 0.4 + row['ctr_lifetime_score'] * 0.6) * 0.5 + (row['ctb_current_score'] * 0.4 + row['ctr_current_score'] * 0.6) * 0.5)
    else:
        return None
    # return float((row['ctb_lifetime_score'] * 0.4 + row['ctr_lifetime_score'] * 0.6) * 0.5 + (row['ctb_current_score'] * 0.4 + row['ctr_current_score'] * 0.6) * 0.5)


def get_final_score(row):
    if row is not None and row.gmv_current_score is not None and row.gmv_lifetime_score is not None and row.buy_quantity_current_score is not None and row.buy_quantity_lifetime_score is not None:
        return float(row['reco_performance_score'] * 0.6 + row['reco_score'] * 0.4)
    else:
        return None
    # return float(row['reco_performance_score'] * 0.6 + row['reco_score'] * 0.4)


def basic_algo(self, model_info, df_datasource, df_datasource_config, df_algorithm_config, model, widget):
    info('running basic algo')
    model_root_element_key = df_algorithm_config.select('model_root_element_key').first().__getitem__("model_root_element_key")
    # print(model_root_element_key)
    home_element_key_id = df_algorithm_config.select('home_element_key_id').first().__getitem__("home_element_key_id")
    # print(home_element_key_id)
    print(df_algorithm_config.select('column_qualifier_name').first().__getitem__("column_qualifier_name"))
    start = time.time()
    ds_gmv = df_datasource['gmv_data']
    ds_ctb = df_datasource['ctb_data']
    print(ds_gmv.show())
    # print(ds_ctb.dtypes)
    # udfreco_score = udf(get_reco_score,DoubleType())
    # ds_gmv.groupby("category_id","product_id").apply(frequencies).show()
    # print(ds_gmv.groupby("category_id","product_id","gmv").apply(frequencies).show())
    # exit()
    
    unique_cat_ids_gmv = ds_gmv.select("category_id").distinct()
    unique_cat_ids_gmv = unique_cat_ids_gmv.select('category_id').rdd.map(lambda row : row[0]).collect()
    # print(len(unique_cat_ids_gmv))

    unique_cat_ids_ctb = ds_ctb.select("category_id").distinct()
    unique_cat_ids_ctb = unique_cat_ids_ctb.select('category_id').rdd.map(lambda row : row[0]).collect()
    # print(len(unique_cat_ids_ctb))
    unique_cat_ids = list(set(unique_cat_ids_gmv+unique_cat_ids_ctb))
    # print(unique_cat_ids)
    for row in unique_cat_ids:
        print('running basic algo for category_id->',row)
        ds_current_gmv = ds_gmv.filter((col(model_root_element_key) == row) & (col('nature') == "current")).select(model_root_element_key,"product_id","gmv")
        ds_current_gmv_freq = frequencies(ds_current_gmv,"gmv","gmv_current_score")
        ds_gmv = ds_gmv.join(ds_current_gmv_freq,on=[model_root_element_key,"product_id"],how="left")
        
        ds_lifetime_gmv = ds_gmv.filter((col(model_root_element_key) == row) & (col('nature') == "lifetime")).select(model_root_element_key,"product_id","gmv")
        ds_lifetime_gmv_freq = frequencies(ds_lifetime_gmv,"gmv","gmv_lifetime_score")
        ds_gmv = ds_gmv.join(ds_lifetime_gmv_freq,on=[model_root_element_key,"product_id"],how="left")

        ds_current_buy_quantity = ds_gmv.filter((col(model_root_element_key) == row) & (col('nature') == "current")).select(model_root_element_key,"product_id","buy_quantity")
        ds_current_buy_quantity_freq = frequencies(ds_current_buy_quantity,"buy_quantity","buy_quantity_current_score")
        ds_gmv = ds_gmv.join(ds_current_buy_quantity_freq,on=[model_root_element_key,"product_id"],how="left")

        ds_lifetime_buy_quantity = ds_gmv.filter((col(model_root_element_key) == row) & (col('nature') == "lifetime")).select(model_root_element_key,"product_id","buy_quantity")
        ds_lifetime_buy_quantity_freq = frequencies(ds_lifetime_buy_quantity,"buy_quantity","buy_quantity_lifetime_score")
        ds_gmv = ds_gmv.join(ds_lifetime_buy_quantity_freq,on=[model_root_element_key,"product_id"],how="left")
        
        ds_lifetime_ctb = ds_ctb.filter((col(model_root_element_key) == row) & (col('nature') == "lifetime") &(col('outlier_flag_ctb_percentage') == 1)).select(model_root_element_key,"product_id","ctb_percentage")
        ds_lifetime_ctb_freq = frequencies(ds_lifetime_ctb,"ctb_percentage","ctb_lifetime_score")
        ds_ctb = ds_ctb.join(ds_lifetime_ctb_freq,on=[model_root_element_key,"product_id"],how="left")

        ds_current_ctb = ds_ctb.filter((col(model_root_element_key) == row) & (col('nature') == "current") &(col('outlier_flag_ctb_percentage') == 1)).select(model_root_element_key,"product_id","ctb_percentage")
        ds_current_ctb_freq = frequencies(ds_current_ctb,"ctb_percentage","ctb_current_score")
        ds_ctb = ds_ctb.join(ds_current_ctb_freq,on=[model_root_element_key,"product_id"],how="left")

        ds_current_ctr = ds_ctb.filter((col(model_root_element_key) == row) & (col('nature') == "current") &(col('outlier_flag_ctr') == 1)).select(model_root_element_key,"product_id","ctr")
        ds_current_ctr_freq = frequencies(ds_current_ctr,"ctr","ctr_current_score")
        ds_ctb = ds_ctb.join(ds_current_ctr_freq,on=[model_root_element_key,"product_id"],how="left")

        ds_lifetime_ctr = ds_ctb.filter((col(model_root_element_key) == row) & (col('nature') == "lifetime") &(col('outlier_flag_ctr') == 1)).select(model_root_element_key,"product_id","ctr")
        ds_lifetime_ctr_freq = frequencies(ds_lifetime_ctr,"ctr","ctr_lifetime_score")
        ds_ctb = ds_ctb.join(ds_lifetime_ctr_freq,on=[model_root_element_key,"product_id"],how="left")
        break
    
    # print(ds_gmv.filter(col('category_id') == 2).show())
    # print(ds_ctb.filter(col('category_id') == 2).show())
    # exit()
    # product_with_gmv_score_current = getRelativeFrequencyFor(df_datasource['gmv_data'][df_datasource['gmv_data']['nature'] == 'current'], model_root_element_key, 'product_id', 'gmv')
    # df_datasource['gmv_data'] = df_datasource['gmv_data'].withColumn('gmv_current_score', lit(0))
    # for cat_id, product_data in product_with_gmv_score_current.items():
    #     for product_with_score in product_data:
    #         df_datasource['gmv_data'].filter((col('product_id') == product_with_score[0])).withColumn("gmv_current_score",  lit(product_with_score[2]))
    
    # exit()
    # product_with_gmv_score_lifetime = getRelativeFrequencyFor(df_datasource['gmv_data'][df_datasource['gmv_data']['nature'] == 'lifetime'], model_root_element_key, 'product_id', 'gmv')
    # df_datasource['gmv_data'] = df_datasource['gmv_data'].withColumn('gmv_lifetime_score', lit(0))
    # for cat_id, product_data in product_with_gmv_score_lifetime.items():
    #     for product_with_score in product_data:
    #         df_datasource['gmv_data'].filter((col('product_id') == product_with_score[0])).withColumn("gmv_lifetime_score",  lit(product_with_score[2]))
    
    # product_with_buy_quantity_score_current = getRelativeFrequencyFor(df_datasource['gmv_data'][df_datasource['gmv_data']['nature'] == 'current'], model_root_element_key, 'product_id', 'buy_quantity')
    # df_datasource['gmv_data'] = df_datasource['gmv_data'].withColumn('buy_quantity_current_score', lit(0))
    # for cat_id, product_data in product_with_buy_quantity_score_current.items():
    #     for product_with_score in product_data:
    #         df_datasource['gmv_data'].filter((col('product_id') == product_with_score[0])).withColumn("buy_quantity_current_score",  lit(product_with_score[2]))
    
    # product_with_buy_quantity_score_lifetime = getRelativeFrequencyFor(df_datasource['gmv_data'][df_datasource['gmv_data']['nature'] == 'lifetime'], model_root_element_key, 'product_id', 'buy_quantity')
    # df_datasource['gmv_data'] = df_datasource['gmv_data'].withColumn('buy_quantity_lifetime_score', lit(0))
    # for cat_id, product_data in product_with_buy_quantity_score_lifetime.items():
    #     for product_with_score in product_data:
    #         df_datasource['gmv_data'].filter((col('product_id') == product_with_score[0])).withColumn("buy_quantity_lifetime_score",  lit(product_with_score[2]))
    
    # product_with_ctb_score_lifetime = getRelativeFrequencyFor(df_datasource['ctb_data'][(df_datasource['ctb_data']['outlier_flag_ctb_percentage'] == 1) & (df_datasource['ctb_data']['nature'] == 'lifetime')], model_root_element_key, 'product_id', 'ctb_percentage')
    # df_datasource['ctb_data'] = df_datasource['ctb_data'].withColumn('ctb_lifetime_score', lit(0))
    # for cat_id, product_data in product_with_ctb_score_lifetime.items():
    #     for product_with_score in product_data:
    #         df_datasource['ctb_data'].filter((col('product_id') == product_with_score[0])).withColumn("ctb_lifetime_score",  lit(product_with_score[2]))
    
    # product_with_ctr_score_lifetime = getRelativeFrequencyFor(df_datasource['ctb_data'][(df_datasource['ctb_data']['outlier_flag_ctr'] == 1) & (df_datasource['ctb_data']['nature'] == 'lifetime')], model_root_element_key, 'product_id', 'ctr')
    # df_datasource['ctb_data'] = df_datasource['ctb_data'].withColumn('ctr_lifetime_score', lit(0))
    # for cat_id, product_data in product_with_ctb_score_lifetime.items():
    #     for product_with_score in product_data:
    #         df_datasource['ctb_data'].filter((col('product_id') == product_with_score[0])).withColumn("ctr_lifetime_score",  lit(product_with_score[2]))
    
    # product_with_ctb_score_lifetime = getRelativeFrequencyFor(df_datasource['ctb_data'][(df_datasource['ctb_data']['outlier_flag_ctb_percentage'] == 1) & (df_datasource['ctb_data']['nature'] == 'current')], model_root_element_key, 'product_id', 'ctb_percentage')
    # df_datasource['ctb_data'] = df_datasource['ctb_data'].withColumn('ctb_current_score', lit(0))
    # for cat_id, product_data in product_with_ctb_score_lifetime.items():
    #     for product_with_score in product_data:
    #         df_datasource['ctb_data'].filter((col('product_id') == product_with_score[0])).withColumn("ctb_current_score",  lit(product_with_score[2]))
    
    # product_with_ctr_score_lifetime = getRelativeFrequencyFor(df_datasource['ctb_data'][(df_datasource['ctb_data']['outlier_flag_ctr'] == 1) & (df_datasource['ctb_data']['nature'] == 'current')], model_root_element_key, 'product_id', 'ctr')
    # df_datasource['ctb_data'] = df_datasource['ctb_data'].withColumn('ctr_current_score', lit(0))
    # for cat_id, product_data in product_with_ctb_score_lifetime.items():
    #     for product_with_score in product_data:
    #         df_datasource['ctb_data'].filter((col('product_id') == product_with_score[0])).withColumn("ctr_current_score",  lit(product_with_score[2]))
    
    df_datasource['product_pool'] = df_datasource['product_pool'].join(ds_gmv.select('product_id', 'gmv_current_score', 'gmv_lifetime_score','buy_quantity_current_score','buy_quantity_lifetime_score'), on=['product_id'],how="left")
    # df_datasource['product_pool'] = pd.merge(df_datasource['product_pool'], df_datasource['gmv_data'][['product_id', 'gmv_current_score', 'gmv_lifetime_score']], on='product_id', how='left')
    # df_datasource['product_pool'] = df_datasource['product_pool'].join(df_datasource['gmv_data'], on=['product_id'],how="left")
    # df_datasource['product_pool'] = pd.merge(df_datasource['product_pool'], df_datasource['gmv_data'][['product_id', 'buy_quantity_current_score', 'buy_quantity_lifetime_score']], on='product_id', how='left')
    df_datasource['product_pool'] = df_datasource['product_pool'].join(ds_ctb.select('product_id','ctb_lifetime_score','ctr_lifetime_score','ctb_current_score','ctr_current_score'), on='product_id', how='left')
    df_datasource['product_pool'] = df_datasource['product_pool'].na.fill(0.0)
    df_final = df_datasource['product_pool']
    # df_datasource['product_pool'][['gmv_current_score',
    #  'gmv_lifetime_score',
    #  'buy_quantity_current_score',
    #  'buy_quantity_lifetime_score',
    #  'ctb_lifetime_score',
    #  'ctr_lifetime_score',
    #  'ctb_current_score',
    #  'ctr_current_score'
    #  ]] = df_datasource['product_pool'].fillna(0.0,subset=['gmv_current_score','gmv_lifetime_score','buy_quantity_current_score','buy_quantity_lifetime_score','ctb_lifetime_score','ctr_lifetime_score','ctb_current_score','ctr_current_score'])

    # df_datasource['product_pool'].fillna(0, subset=['gmv_current_score', 'gmv_lifetime_score',''])
    # print(df_final.show())
    udfreco_score = udf(get_reco_score,DoubleType())
    udfreco_perfomence_score = udf(get_perfomence_score,DoubleType())
    udfreco_final_score = udf(get_final_score,DoubleType())
    df_final = df_final.withColumn("reco_score", udfreco_score(struct([df_final[x] for x in df_final.columns])))
    df_final = df_final.withColumn("reco_performance_score", udfreco_perfomence_score(struct([df_final[x] for x in df_final.columns])))
    df_final = df_final.withColumn("final_reco_score", udfreco_final_score(struct([df_final[x] for x in df_final.columns])))
    df_final = df_final.na.drop()

    print(df_final.printSchema())
    end = time.time()
    print "\nExecution of job %s took %s seconds" % ("gmv_data",end-start)
    df_datasource['product_pool'] = df_final
    # print(df_final.show())
    # exit()
    # df_datasource['product_pool']['reco_score'] = df_datasource['product_pool'].apply(lambda row: get_reco_score(row), axis=1)
    # df_datasource['product_pool']['reco_performance_score'] = df_datasource['product_pool'].apply(lambda row: get_perfomence_score(row), axis=1)
    # df_datasource['product_pool']['final_reco_score'] = df_datasource['product_pool'].apply(lambda row: get_final_score(row), axis=1)
    
    df_datasource['product_pool'] = df_datasource['product_pool'].drop_duplicates()
    df_datasource['product_pool'].filter((col('final_reco_score') < 1)).withColumn(mapping_dic[model_root_element_key],  lit(0))
    # df_datasource['product_pool'].loc[df_datasource['product_pool']['final_reco_score'] <  1, mapping_dic[model_root_element_key]] = 0
    df_datasource['product_pool'] = df_datasource['product_pool'].select('product_id',model_root_element_key,df_algorithm_config.select('column_qualifier_name').first().__getitem__("column_qualifier_name")+'_name', mapping_dic[model_root_element_key],'final_reco_score').orderBy([model_root_element_key,'final_reco_score'], ascending=[True, False])

    # df_datasource['product_pool'] = df_datasource['product_pool'][['product_id', model_root_element_key,df_algorithm_config.select('column_qualifier_name').first().__getitem__("column_qualifier_name")+'_name', mapping_dic[model_root_element_key],'final_reco_score']].orderBy(["model_root_element_key",final_reco_score], ascending=[True, False]).reset_index(drop=True)
    df_datasource['product_pool']['RN'] = df_datasource['product_pool'].groupby([model_root_element_key,df_algorithm_config.select('column_qualifier_name').first().__getitem__("column_qualifier_name")+'_name',mapping_dic[model_root_element_key]])['final_reco_score'].rank(method='first', ascending=False)
    df_datasource['product_pool']=df_datasource['product_pool'].orderBy(["RN","final_reco_score"],ascending=[True,False]).reset_index(drop=True)    
#    df_datasource['product_pool']['RN'] = df_datasource['product_pool'].groupby([model_root_element_key,df_algorithm_config['column_qualifier_name'][0]+'_name'])['final_reco_score'].rank(method='first', ascending=False)

    #df_datasource['product_pool_for_home']=df_datasource['product_pool_for_home'].drop(["level_0","index"],axis=1).reset_index(drop=True)
    #df_home_product_pool_redorder=UT.order_products_by_business_rule(df_datasource['product_pool_for_home'].copy(),model_root_element_key+"_reference_id",ascending=True)
    #df_datasource['product_pool_for_home']=df_home_product_pool_redorder
        
    df_datasource['product_pool'][model_root_element_key+"_reference_id"] = df_datasource['product_pool'][model_root_element_key]
    
    #df_datasource['product_pool'] = df_datasource['product_pool'].append(df_datasource['product_pool_for_home'])
    df_datasource['product_pool']['product_id'] = df_datasource['product_pool']['product_id'].astype(int)
    #df_datasource['product_pool'].loc[df_datasource['product_pool']['final_reco_score'] <  1, 'RN'] = 10000

    df_data_model = df_datasource['product_pool'].orderBy(["RN","final_reco_score"],ascending=[True,False]).reset_index(drop=True).copy()
    
    #Evaluation
    df_evaluation_data = SQL.fetch_dataframe("select * from model_evaluation_data where widget_id='{}'".format(widget), "warehouse")
    df_data_model = EV.get_final_score(df_data_model, df_evaluation_data, model_root_element_key, n=100)
    
    #Home products
    home_categories_list = list(map(int, home_element_key_id.split(',')))
    info(str(home_categories_list))
    df_datasource['product_pool_for_home'] = df_data_model.query('final_score <  8 and ' + model_root_element_key + '_reference_id in ' + str(home_categories_list))
    info(df_datasource['product_pool_for_home'])
    #df_datasource['product_pool_for_home'][model_root_element_key+"_reference_id"] = df_datasource['product_pool_for_home'][model_root_element_key]

    df_datasource['product_pool_for_home'][model_root_element_key] = 'home'
    df_datasource['product_pool_for_home'][df_algorithm_config['column_qualifier_name'][0]+'_name'] = 'home'

    #df_datasource['product_pool_for_home']['RN'] = df_datasource['product_pool_for_home'].groupby(model_root_element_key)['final_reco_score'].rank(ascending=False)

    
    #df_data_model = EV.get_clusterwise_evaluation_rank(df_data_model, widget, n=100)
    df_datasource['product_pool_for_home'] = UT.order_products_by_business_rule(df_datasource['product_pool_for_home'].sort_values(["final_score","final_rank","final_reco_score"],ascending=[True,True,False]), model_root_element_key+"_reference_id",ascending=True)
    if(not df_datasource['product_pool_for_home'].empty):
        df_datasource['product_pool_for_home']["final_score"] = df_datasource['product_pool_for_home'].reset_index(drop=True).reset_index()["index"].rank()
    df_datasource['product_pool'] = df_data_model.query(model_root_element_key+" != 'home'")
    df_data_model = df_datasource['product_pool'].append(df_datasource['product_pool_for_home'])
    df_data_model = df_data_model.sort_values(["final_score","final_rank","final_reco_score"],ascending=[True,True,False]).reset_index(drop=True).copy()
    df_data_model.drop("RN", axis=1, inplace=True)
    df_data_model.rename(columns={"final_score":"RN"}, inplace=True)

    # push_to_db(self, model_info, df_data_model, df_algorithm_config, model, widget)


def eval_algo(self, model_info, df_datasource, df_datasource_config, df_algorithm_config):
    info('eval')


def push_to_db(self, model_info, df_data_model, df_algorithm_config, model, widget):
    info('pushing in to bigTable')
    row_key = df_algorithm_config['widget'][0]+'#uc1'
    column_family = df_algorithm_config['column_family'][0]
    column_qualifier_name = df_algorithm_config['column_qualifier_name'][0]
    column_qualifier_id = df_algorithm_config['model_root_element_key'][0]
    model_root_element_key = df_algorithm_config['model_root_element_key'][0]
    info('pushing data for :' + model_root_element_key)
    sort_keys = df_algorithm_config['sort_keys'][0].split(',')
    sort_keys_ascending_order = df_algorithm_config['sort_keys_ascending_order'][0].split(',')
    sort_keys_ascending_order = list(np.array([ ast.literal_eval(x.title()) for x in sort_keys_ascending_order ]))
    df_data_model['row_key'] = row_key
    ordering_score_key = df_algorithm_config['ordering_score_key'][0]
    BT.push_to_bigtable(df_data_model, df_algorithm_config, widget, column_family, column_qualifier_name, column_qualifier_id, sort_keys, sort_keys_ascending_order, model_root_element_key, ordering_score_key,push_user_info=False,push_response_data_for_fallback=True)


# In[ ]:



