
# coding: utf-8

# In[1]:

import pandas as pd
import time
import redis
from flask import current_app
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
import sys, os
sys.path.append(os.environ['ENGINE_HOME'])
import helper.mysql.get_connection as SQL
import helper.mysql.get_connection as SQL
import helper.google_cloud.bigTable_connector as BT
import ast
import numpy as np
import helper.lib.utilities as UT 


def info(msg):
    current_app.logger.info(msg)


def frequencies(counts):
    counts = sorted(list(map(int, set(counts))), reverse=False)
    for item in counts[:]:
        if item < 1:
            counts.remove(item)

    total = float(len(counts))
    element_frequency = {}
    frequencies = []
    sequence_number = 1
    for s in counts:
        element_frequency[s] = float(sequence_number) / float(total) * 100
        sequence_number += 1

    element_frequency[0] = 0
    return element_frequency


mapping_dic={"category_id":"brand_id","brand_id":"category_id"}

def getRelativeFrequencyFor(ds, for_key, on_key, value_key):
    root_element_id_with_products = {}
    for idx, row in ds.iterrows():
        if row[for_key] not in root_element_id_with_products:
            root_element_id_with_products[row[for_key]] = []
        root_element_id_with_products[row[for_key]].append([row[on_key], row[value_key]])

    for cat_key, cat_products in root_element_id_with_products.items():
        rank_score = []
        for product_details in cat_products:
            rank_score.append(product_details[1])

        rank_score_frequencies = frequencies(rank_score)
        product_details_with_frequency_score = []
        for product_details in cat_products:
            product_details.append(rank_score_frequencies[product_details[1]])
            product_details_with_frequency_score.append(product_details)

        root_element_id_with_products[cat_key] = product_details_with_frequency_score

    return root_element_id_with_products


def get_reco_score(row):
    return float((row['gmv_current_score'] * 0.25 + row['gmv_lifetime_score'] * 0.25 + row['buy_quantity_current_score'] * 0.25 + row['buy_quantity_lifetime_score'] * 0.25) / 100)


def get_perfomence_score(row):
    return float((row['ctb_lifetime_score'] * 0.4 + row['ctr_lifetime_score'] * 0.6) * 0.5 + (row['ctb_current_score'] * 0.4 + row['ctr_current_score'] * 0.6) * 0.5)


def get_final_score(row):
    return float(row['reco_performance_score'] * 0.6 + row['reco_score'] * 0.4)


def basic_algo(self, model_info, df_datasource, df_datasource_config, df_algorithm_config, model, widget):
    info('running basic algo')
    model_root_element_key = df_algorithm_config['model_root_element_key'][0]
    home_element_key_id = df_algorithm_config['home_element_key_id'][0]
    product_with_gmv_score_current = getRelativeFrequencyFor(df_datasource['gmv_data'][df_datasource['gmv_data']['nature'] == 'current'], model_root_element_key, 'product_id', 'gmv')
    df_datasource['gmv_data']['gmv_current_score'] = 0
    for cat_id, product_data in product_with_gmv_score_current.items():
        for product_with_score in product_data:
            df_datasource['gmv_data'].loc[df_datasource['gmv_data']['product_id'] == product_with_score[0], 'gmv_current_score'] = product_with_score[2]

    product_with_gmv_score_lifetime = getRelativeFrequencyFor(df_datasource['gmv_data'][df_datasource['gmv_data']['nature'] == 'lifetime'], model_root_element_key, 'product_id', 'gmv')
    df_datasource['gmv_data']['gmv_lifetime_score'] = 0
    for cat_id, product_data in product_with_gmv_score_lifetime.items():
        for product_with_score in product_data:
            df_datasource['gmv_data'].loc[df_datasource['gmv_data']['product_id'] == product_with_score[0], 'gmv_lifetime_score'] = product_with_score[2]

    product_with_buy_quantity_score_current = getRelativeFrequencyFor(df_datasource['gmv_data'][df_datasource['gmv_data']['nature'] == 'current'], model_root_element_key, 'product_id', 'buy_quantity')
    df_datasource['gmv_data']['buy_quantity_current_score'] = 0
    for cat_id, product_data in product_with_buy_quantity_score_current.items():
        for product_with_score in product_data:
            df_datasource['gmv_data'].loc[df_datasource['gmv_data']['product_id'] == product_with_score[0], 'buy_quantity_current_score'] = product_with_score[2]

    product_with_buy_quantity_score_lifetime = getRelativeFrequencyFor(df_datasource['gmv_data'][df_datasource['gmv_data']['nature'] == 'lifetime'], model_root_element_key, 'product_id', 'buy_quantity')
    df_datasource['gmv_data']['buy_quantity_lifetime_score'] = 0
    for cat_id, product_data in product_with_buy_quantity_score_lifetime.items():
        for product_with_score in product_data:
            df_datasource['gmv_data'].loc[df_datasource['gmv_data']['product_id'] == product_with_score[0], 'buy_quantity_lifetime_score'] = product_with_score[2]

    product_with_ctb_score_lifetime = getRelativeFrequencyFor(df_datasource['ctb_data'][(df_datasource['ctb_data']['outlier_flag_ctb_percentage'] == 1) & (df_datasource['ctb_data']['nature'] == 'lifetime')], model_root_element_key, 'product_id', 'ctb_percentage')
    df_datasource['ctb_data']['ctb_lifetime_score'] = 0
    for cat_id, product_data in product_with_ctb_score_lifetime.items():
        for product_with_score in product_data:
            df_datasource['ctb_data'].loc[df_datasource['ctb_data']['product_id'] == product_with_score[0], 'ctb_lifetime_score'] = product_with_score[2]

    product_with_ctr_score_lifetime = getRelativeFrequencyFor(df_datasource['ctb_data'][(df_datasource['ctb_data']['outlier_flag_ctr'] == 1) & (df_datasource['ctb_data']['nature'] == 'lifetime')], model_root_element_key, 'product_id', 'ctr')
    df_datasource['ctb_data']['ctr_lifetime_score'] = 0
    for cat_id, product_data in product_with_ctb_score_lifetime.items():
        for product_with_score in product_data:
            df_datasource['ctb_data'].loc[df_datasource['ctb_data']['product_id'] == product_with_score[0], 'ctr_lifetime_score'] = product_with_score[2]

    product_with_ctb_score_lifetime = getRelativeFrequencyFor(df_datasource['ctb_data'][(df_datasource['ctb_data']['outlier_flag_ctb_percentage'] == 1) & (df_datasource['ctb_data']['nature'] == 'current')], model_root_element_key, 'product_id', 'ctb_percentage')
    df_datasource['ctb_data']['ctb_current_score'] = 0
    for cat_id, product_data in product_with_ctb_score_lifetime.items():
        for product_with_score in product_data:
            df_datasource['ctb_data'].loc[df_datasource['ctb_data']['product_id'] == product_with_score[0], 'ctb_current_score'] = product_with_score[2]

    product_with_ctr_score_lifetime = getRelativeFrequencyFor(df_datasource['ctb_data'][(df_datasource['ctb_data']['outlier_flag_ctr'] == 1) & (df_datasource['ctb_data']['nature'] == 'current')], model_root_element_key, 'product_id', 'ctr')
    df_datasource['ctb_data']['ctr_current_score'] = 0
    for cat_id, product_data in product_with_ctb_score_lifetime.items():
        for product_with_score in product_data:
            df_datasource['ctb_data'].loc[df_datasource['ctb_data']['product_id'] == product_with_score[0], 'ctr_current_score'] = product_with_score[2]

    df_datasource['product_pool'] = pd.merge(df_datasource['product_pool'], df_datasource['gmv_data'][['product_id', 'gmv_current_score', 'gmv_lifetime_score']], on='product_id', how='left')
    df_datasource['product_pool'] = pd.merge(df_datasource['product_pool'], df_datasource['gmv_data'][['product_id', 'buy_quantity_current_score', 'buy_quantity_lifetime_score']], on='product_id', how='left')
    df_datasource['product_pool'] = pd.merge(df_datasource['product_pool'], df_datasource['ctb_data'][['product_id',
     'ctb_lifetime_score',
     'ctr_lifetime_score',
     'ctb_current_score',
     'ctr_current_score']], on='product_id', how='left')
    df_datasource['product_pool'][['gmv_current_score',
     'gmv_lifetime_score',
     'buy_quantity_current_score',
     'buy_quantity_lifetime_score',
     'ctb_lifetime_score',
     'ctr_lifetime_score',
     'ctb_current_score',
     'ctr_current_score']] = df_datasource['product_pool'][['gmv_current_score',
     'gmv_lifetime_score',
     'buy_quantity_current_score',
     'buy_quantity_lifetime_score',
     'ctb_lifetime_score',
     'ctr_lifetime_score',
     'ctb_current_score',
     'ctr_current_score']].fillna(0.0)
    df_datasource['product_pool']['reco_score'] = df_datasource['product_pool'].apply(lambda row: get_reco_score(row), axis=1)
    df_datasource['product_pool']['reco_performance_score'] = df_datasource['product_pool'].apply(lambda row: get_perfomence_score(row), axis=1)
    df_datasource['product_pool']['final_reco_score'] = df_datasource['product_pool'].apply(lambda row: get_final_score(row), axis=1)
    df_datasource['product_pool'] = df_datasource['product_pool'].drop_duplicates()
    df_datasource['product_pool'].loc[df_datasource['product_pool']['final_reco_score'] <  1, mapping_dic[model_root_element_key]] = 0
    df_datasource['product_pool'] = df_datasource['product_pool'][['product_id', model_root_element_key,df_algorithm_config['column_qualifier_name'][0]+'_name', mapping_dic[model_root_element_key],'final_reco_score']].sort_values([model_root_element_key, 'final_reco_score'], ascending=[True, False]).reset_index()
    df_datasource['product_pool']['RN'] = df_datasource['product_pool'].groupby([model_root_element_key,df_algorithm_config['column_qualifier_name'][0]+'_name',mapping_dic[model_root_element_key]])['final_reco_score'].rank(method='first', ascending=False)
    df_datasource['product_pool']=df_datasource['product_pool'].sort_values(["RN","final_reco_score"],ascending=[True,False]).reset_index()    
#    df_datasource['product_pool']['RN'] = df_datasource['product_pool'].groupby([model_root_element_key,df_algorithm_config['column_qualifier_name'][0]+'_name'])['final_reco_score'].rank(method='first', ascending=False)

    home_categories_list = list(map(int, home_element_key_id.split(',')))
    info(str(home_categories_list))
    df_datasource['product_pool_for_home'] = df_datasource['product_pool'].query('RN <  8 and ' + model_root_element_key + ' in ' + str(home_categories_list))
    info(df_datasource['product_pool_for_home'])
    df_datasource['product_pool_for_home'][model_root_element_key+"_reference_id"] = df_datasource['product_pool_for_home'][model_root_element_key]

    df_datasource['product_pool_for_home'][model_root_element_key] = 'home'

    df_datasource['product_pool_for_home']['RN'] = df_datasource['product_pool_for_home'].groupby(model_root_element_key)['final_reco_score'].rank(method='first', ascending=False)

    df_datasource['product_pool_for_home']=df_datasource['product_pool_for_home'].drop(["level_0","index"],axis=1).reset_index(drop=True)
    df_home_product_pool_redorder=UT.order_products_by_business_rule(df_datasource['product_pool_for_home'].copy(),model_root_element_key+"_reference_id",ascending=True)
    df_datasource['product_pool_for_home']=df_home_product_pool_redorder
    
    df_datasource['product_pool_for_home'][df_algorithm_config['column_qualifier_name'][0]+'_name'] = 'home'
    
    df_datasource['product_pool'] = df_datasource['product_pool'].append(df_datasource['product_pool_for_home'])
    df_datasource['product_pool']['product_id'] = df_datasource['product_pool']['product_id'].astype(int)
    df_datasource['product_pool'].loc[df_datasource['product_pool']['final_reco_score'] <  1, 'RN'] = 10000

    df_data_model = df_datasource['product_pool'].sort_values(["RN","final_reco_score"],ascending=[True,False]).drop("level_0",axis=1).reset_index().copy()
    push_to_db(self, model_info, df_data_model, df_algorithm_config, model, widget)


def eval_algo(self, model_info, df_datasource, df_datasource_config, df_algorithm_config):
    info('eval')


def push_to_db(self, model_info, df_data_model, df_algorithm_config, model, widget):
    info('pushing in to bigTable')
    row_key = df_algorithm_config['widget'][0]+'#uc1'
    column_family = df_algorithm_config['column_family'][0]
    column_qualifier_name = df_algorithm_config['column_qualifier_name'][0]
    column_qualifier_id = df_algorithm_config['model_root_element_key'][0]
    model_root_element_key = df_algorithm_config['model_root_element_key'][0]
    info('pushing data for :' + model_root_element_key)
    sort_keys = df_algorithm_config['sort_keys'][0].split(',')
    sort_keys_ascending_order = df_algorithm_config['sort_keys_ascending_order'][0].split(',')
    sort_keys_ascending_order = list(np.array([ ast.literal_eval(x.title()) for x in sort_keys_ascending_order ]))
    df_data_model['row_key'] = row_key
    ordering_score_key = df_algorithm_config['ordering_score_key'][0]
    BT.push_to_bigtable(df_data_model, df_algorithm_config, widget, column_family, column_qualifier_name, column_qualifier_id, sort_keys, sort_keys_ascending_order, model_root_element_key, ordering_score_key,push_user_info=False,push_response_data_for_fallback=True)


# In[ ]:



