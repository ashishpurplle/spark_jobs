
# coding: utf-8

# In[1]:

import pandas as pd
#pd.options.display.max_colwidth = 10000
import time
import redis
from flask import current_app
import sys,os
sys.path.append(os.environ['ENGINE_HOME'])
import helper.mysql.get_connection  as SQL

#import  src.main.algorithm as AL
import ast
import numpy as np 
import helper.google_cloud.bigTable_connector  as BT
from google.cloud.bigtable import column_family

import json,datetime


# In[2]:

def importDataInChuncks(df_data,table,max_rows):
    total_count=df_data.shape[0]
    total_splits=int(total_count/max_rows)+1
    df_splits = np.array_split(df_data, total_splits)
    result=False
    for df_chunk in df_splits :
        print("pushing chunck : "+ str(df_chunk.shape[0]))
        result=table.mutate_rows(df_chunk['row_id'].tolist())
    return result


# In[3]:

def insert_user_cluster_data(widget, model, entity=None):
    #get all required data 
    df_datasource_config=SQL.fetch_data_with_schema("select * from ml_datasource_config where widget='"+widget+"' and model='"+model+"'","warehouse")
    df_algorithm_config=SQL.fetch_data_with_schema("select * from ml_algorithm_config where widget='"+widget+"' and model='"+model+"'","warehouse")
    model_root_element_key=df_algorithm_config['model_root_element_key'][0]
    
    #df_user_data=SQL.fetch_data_with_schema("select distinct user_id from customer_master where user_id in ('1314259', '2087301', '1253322', '1304841', '17033','388077','419613','511926','552755','564788','592051','603832','679490','721200','737561','744162','780326','783320','792214','804772','804935','807427','838310','941274','970671','12','646','1147683','304020','248683','93336','326545','597681','391965','591607','967284','628166','910990','259251','1475552','369834','456996','607361','580954','912613')","warehouse")
    #df_user_data=SQL.fetch_data_with_schema("select distinct user_id from customer_master","warehouse")
    #df_user_data=SQL.fetch_data_with_schema("select user_id from customer_master where user_id in ('1314259', '2087301', '1253322', '1304841','17033','388077','419613','511926','552755','564788','592051','603832','679490','721200','737561','744162','780326','783320','792214','804772','804935','807427','838310','941274','970671','12','646','1147683','304020','248683','93336','326545','597681','391965','591607','967284','628166','910990','259251','1475552','369834','456996','607361','580954','912613','2203807','1692013','505411','2041840','1431632','1226283','2461009','818634','1506086')","warehouse")
    df_user_data=SQL.fetch_data_with_schema("select user_id from sample_users_for_delta_training", "warehouse")
    
    df_products = SQL.fetch_dataframe("select category_id, brand_id from product_master where active=1 group by category_id, brand_id", "warehouse")
    df_products = df_products.append([{"category_id":"home","brand_id":"home"}]).astype('str')
    #
    project_id="datapipelinedev"
    instance_id="reco-dev"
    table_name=os.environ["user_cluster_info"]
    table=BT.getBigTableInstance(table_name)

    #df_user_data["cluster_id"] = widget+"#uc1"
    df_user_data['cluster_id']=df_user_data.apply(lambda row : json.dumps({"cluster_id":widget+'#uc1'}),axis=1)
    column_family=df_algorithm_config['column_family'][0]
    
    # Creating Column Family if not exists
    if not column_family in table.list_column_families().keys():
        table.column_family(column_family).create()
    #print(df_algorithm_config['column_qualifier_name'][0])
    
    if(entity != None):
        for entity_id in df_products[entity].unique().tolist():
            print(entity_id)
            #df_user_data['cluster_id']=widget+"#u_"+df_user_data['user_id'].astype(str)

            df_user_data['column_qualifier_id']=df_algorithm_config['column_qualifier_name'][0]+'#'+entity_id

            df_response_data=df_user_data.copy()
            df_response_data['row_id']=df_response_data.apply(lambda x : table.row(str(x['user_id']).encode('utf-8')),axis=1)
            print("pushing total : " +str(df_response_data.shape[0]) +" for algo "+ widget) 
            df_response_data['row_set_value']=df_response_data.apply(lambda x : x['row_id'].set_cell(column_family,
                         x['column_qualifier_id'],
                         x['cluster_id'],
                         timestamp=datetime.datetime.utcnow()),axis=1)
            #print(df_response_data.head(1))
            importDataInChuncks(df_response_data,table,100000)


# In[ ]:

insert_user_cluster_data("bs", "bs", "category_id")

